﻿namespace Task2.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CreditCard
    {
        public int CreditCardID { get; set; }

        [Required]
        [StringLength(16)]
        public string CreditCardNumber { get; set; }

        public int EmployeeID { get; set; }

        [StringLength(40)]
        public string CardHolder { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ExpirationDate { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
