namespace Task2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Northwind_11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCards",
                c => new
                    {
                        CreditCardID = c.Int(nullable: false, identity: true),
                        CreditCardNumber = c.String(nullable: false, maxLength: 16),
                        EmployeeID = c.Int(nullable: false),
                        CardHolder = c.String(maxLength: 40),
                        ExpirationDate = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => t.CreditCardID)
                .ForeignKey("dbo.Employees", t => t.EmployeeID)
                .Index(t => t.EmployeeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CreditCards", "EmployeeID", "dbo.Employees");
            DropIndex("dbo.CreditCards", new[] { "EmployeeID" });
            DropTable("dbo.CreditCards");
        }
    }
}
