using Task2.Model;

namespace Task2.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Task2.Model.Northwind>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Task2.Northwind";
        }

        protected override void Seed(Task2.Model.Northwind context)
        {
            context.Categories.AddOrUpdate(category => category.CategoryID,
                new Category
                {
                    CategoryID = 1,
                    CategoryName = "1",
                    Description = "1"
                }, new Category
                {
                    CategoryID = 2,
                    CategoryName = "2",
                    Description = "2"
                }, new Category
                {
                    CategoryID = 3,
                    CategoryName = "3",
                    Description = "3"
                }, new Category
                {
                    CategoryID = 4,
                    CategoryName = "4",
                    Description = "4"
                }
            );

            context.Regions.AddOrUpdate(region => region.RegionID,
                new Region
                {
                    RegionID = 1,
                    RegionDescription = "1"
                }, new Region
                {
                    RegionID = 2,
                    RegionDescription = "2"
                }, new Region
                {
                    RegionID = 3,
                    RegionDescription = "3"
                }, new Region
                {
                    RegionID = 4,
                    RegionDescription = "4"
                }
            );

            context.Territories.AddOrUpdate(territory => territory.TerritoryID,
                new Territory
                {
                    TerritoryID = "01581",
                    TerritoryDescription = "1",
                    RegionID = 1
                }, new Territory
                {
                    TerritoryID = "01730",
                    TerritoryDescription = "2",
                    RegionID = 2
                }, new Territory
                {
                    TerritoryID = "01833",
                    TerritoryDescription = "3",
                    RegionID = 3
                }, new Territory
                {
                    TerritoryID = "02116",
                    TerritoryDescription = "4",
                    RegionID = 4
                }
            );
        }
    }
}
