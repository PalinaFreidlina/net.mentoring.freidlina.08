namespace Task2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Northwind_13 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Region", newName: "Regions");
            AddColumn("dbo.Customers", "DateCreated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "DateCreated");
            RenameTable(name: "dbo.Regions", newName: "Region");
        }
    }
}
