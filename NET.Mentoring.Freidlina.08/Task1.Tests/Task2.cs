﻿using System;
using System.Diagnostics;
using System.Linq;
using DataModels;
using LinqToDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task1.Tests
{
    [TestClass]
    public class Task2
    {
        [TestMethod]
        public void ListOfProductsWithCategoryAndSupplier()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                foreach (var product in db.Products.LoadWith(t => t.Supplier).LoadWith(t => t.Category))
                {
                    Debug.WriteLine($"---------------\n{product.ProductName}\n{product.Category?.CategoryName}\n{product.Supplier?.CompanyName}");
                }
            }
        }

        [TestMethod]
        public void ListOfEmployeesWithRegion()
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            using (var db = new NorthwindDB("Northwind"))
            {
                var q = db.Employees
                    .Join(db.EmployeeTerritories
                            .Join(db.Territories.LoadWith(t => t.Region),
                                empTerritories => empTerritories.TerritoryID,
                                territories => territories.TerritoryID,
                                (empTerritories, territories) => new
                                {
                                    empTerritories.EmployeeID,
                                    territories.Region.RegionDescription
                                }),
                        employees => employees.EmployeeID,
                        e => e.EmployeeID,
                        (employees, e) => new
                        {
                            employees.FirstName,
                            Region = e.RegionDescription
                        })
                    .GroupBy(key => key.FirstName,
                        (s, enumerable) => new
                        {
                            FirstName = s,
                            Regions = enumerable.Where(item => item.FirstName == s).ToList().Select(item => item.Region).Distinct()
                        });
                foreach (var employee in q)
                {
                    Debug.WriteLine($"---------------\n{employee.FirstName}:");
                    foreach (var region in employee.Regions)
                    {
                        Debug.WriteLine($"{region}");
                    }
                }
            }
        }

        [TestMethod]
        public void StatisticsForRegion()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                var q = db.Employees
                    .Join(db.EmployeeTerritories
                            .Join(db.Territories.LoadWith(t => t.Region),
                                empTerritories => empTerritories.TerritoryID,
                                territories => territories.TerritoryID,
                                (empTerritories, territories) => new
                                {
                                    empTerritories.EmployeeID,
                                    territories.Region.RegionDescription
                                }),
                        employees => employees.EmployeeID,
                        e => e.EmployeeID,
                        (employees, e) => new
                        {
                            employees.FirstName,
                            Region = e.RegionDescription
                        })
                    .GroupBy(key => key.Region,
                        (s, enumerable) => new
                        {
                            Region = s,
                            Count = enumerable.Count(item => item.Region == s)
                        });
                foreach (var region in q)
                {
                    Debug.WriteLine($"---------------\n{region.Region}");
                    Debug.WriteLine($"{region.Count}");
                }
            }
        }

        [TestMethod]
        public void EmploeeShippers()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
                var q = db.Orders
                    .LoadWith(t => t.Employee)
                    .LoadWith(t => t.Shipper)
                    .Select(item => new {item.Employee.FirstName, item.Shipper.CompanyName})
                    .GroupBy(key => key.FirstName, (s, enumerable) => new
                    {
                        FirstName = s,
                        Shippers = enumerable.Where(item => item.FirstName == s).Select(item => item.CompanyName)
                            .Distinct().ToList()
                    });
                foreach (var employee in q)
                {
                    Debug.WriteLine($"---------------\n{employee.FirstName}");
                    foreach (var shipper in employee.Shippers)
                    {
                        Debug.WriteLine($"{shipper}");
                    }
                }
            }
        }
    }
}
