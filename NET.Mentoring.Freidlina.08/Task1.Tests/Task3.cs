﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using LinqToDB;
using LinqToDB.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task1.Tests
{
    [TestClass]
    public class Task3
    {
        [TestMethod]
        public void InsertNewEmployee()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                var empId = Convert.ToInt32(db.InsertWithIdentity(new Employee { FirstName = "John", LastName = "Green" }));
                db.EmployeeTerritories
                    .Value(et => et.EmployeeID, empId)
                    .Value(et => et.TerritoryID, "10019")
                    .Insert();
            }
        }

        [TestMethod]
        public void UpdateProductsCategory()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                db.Products
                  .Where(p => p.CategoryID == 7)
                  .Set(p => p.CategoryID, 6)
                  .Update();
            }
        }

        [TestMethod]
        public void InsertProducts()
        {
            using (var db = new NorthwindDB("Northwind"))
            {
                var list = new List<Product>
                {
                    new Product
                    {
                        Category = new Category
                        {
                            CategoryName = "Produce"
                        },
                        Supplier = new Supplier
                        {
                            CompanyName = "Mayumi's"
                        },
                        ProductName = "Product1"
                    },
                    new Product
                    {
                        Category = new Category
                        {
                            CategoryName = "Produce"
                        },
                        Supplier = new Supplier
                        {
                            CompanyName = "Mayumi's1"
                        },
                        ProductName = "Product2"
                    },
                    new Product
                    {
                        Category = new Category
                        {
                            CategoryName = "Produce1"
                        },
                        Supplier = new Supplier
                        {
                            CompanyName = "Mayumi's2"
                        },
                        ProductName = "Product3"
                    }
                };
                db.Categories
                    .Merge()
                    .Using(list.Select(item => item.Category))
                    .On(item => item.CategoryName, item => item.CategoryName)
                    .InsertWhenNotMatched(source => new Category
                    {
                        CategoryName = source.CategoryName
                    })
                    .Merge();
                db.Suppliers
                    .Merge()
                    .Using(list.Select(item => item.Supplier))
                    .On(item => item.CompanyName, item => item.CompanyName)
                    .InsertWhenNotMatched(source => new Supplier
                    {
                        CompanyName = source.CompanyName
                    })
                    .Merge();
                list.ForEach(item => item.CategoryID = db.Categories.First(ct => ct.CategoryName == item.Category.CategoryName).CategoryID);
                list.ForEach(item => item.SupplierID = db.Suppliers.First(ct => ct.CompanyName == item.Supplier.CompanyName).SupplierID);
                db.Products
                    .Merge()
                    .Using(list)
                    .On(pr => pr.ProductName, pr => pr.ProductName)
                    .InsertWhenNotMatched(products => new Product
                    {
                        ProductName = products.ProductName,
                        CategoryID = products.CategoryID,
                        SupplierID = products.SupplierID
                    })
                    .Merge();
            }
        }

        [TestMethod]
        public void ProductsReplacement()
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            var db = new NorthwindDB("Northwind");
            var orders = db.OrderDetails.LoadWith(t => t.Order)
                .Where(order => order.Order.ShippedDate == null)
                .GroupBy(key => key.OrderID,
                        (i, detailses) => new
                        {
                            OrderID = i,
                            PrductIDs = detailses.Where(item => item.OrderID == i).Select(item => item.ProductID).ToList()
                        }).ToList();
            db.Dispose();
            var rdm = new Random(DateTime.Now.Millisecond);
            using (db = new NorthwindDB("Northwind"))
            {
                foreach (var order in orders)
                {
                    int productid;
                    do
                    {
                        productid = rdm.Next(1, 77);
                    } while (order.PrductIDs.Contains(productid));
                    db.OrderDetails
                        .Where(item => item.OrderID == order.OrderID && item.ProductID == order.PrductIDs.First())
                        .Set(od => od.ProductID, productid)
                        .Set(od => od.Discount, (float)0.99)
                        .Update();
                }
            }
        }
    }
}
