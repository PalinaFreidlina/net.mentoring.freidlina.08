﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task2.Model;

namespace Task2.Tests
{
    [TestClass]
    public class Task1
    {
        [TestMethod]
        public void ListOfOrdersAndItsProductsByCategory()
        {
            using (var db = new Northwind())
            {
                var q = db.Order_Details.Select(item => new {item.Product.Category.CategoryName, item.Order}).GroupBy(
                    key => key.CategoryName, (categoryname, enumerable) => new
                    {
                        Category = categoryname,
                        Orders = enumerable.Where(item => item.CategoryName == categoryname).Select(item => item.Order)
                            .ToList()
                    });
                foreach (var category in q)
                {
                    Debug.WriteLine("Category: " + category.Category + " ------");
                    foreach (var order in category.Orders)
                    {
                        Debug.WriteLine("Order: " + order.OrderID + " ------");
                        foreach (var product in order.Order_Details.Select(item => item.Product).Where(item => item.Category.CategoryName == category.Category))
                        {
                            Debug.WriteLine("Product: " + product.ProductName);
                        }
                    }
                }
            }
        }
    }
}
